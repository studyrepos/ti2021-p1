#!/bin/bash

file=./res/example.txt
rebuild=false

for arg in "$@";
do
  case "$arg" in
  -b) rebuild=true
      ;;
  *)  file="$arg"
      ;;
  esac
done

if [ ! -d "build" ]
then
  rebuild=true
fi

if [ $rebuild == true ]
then
  echo "Rebuild..."
  rm -r build && mkdir build

  res_cc=$(javacc -OUTPUT_DIRECTORY:./build ./src/While2URM.jj)

  if [[ $res_cc =~ errors ]]
  then
    echo "Error detected"
    echo "$res_cc"
    exit 1
  else
    echo "JavaCC build without errors"
  fi
  javac -d ./build -sourcepath ./build ./build/While2URM.java
fi

java -classpath build/ While2URM "$file"
