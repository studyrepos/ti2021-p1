### Theoretische Informatik (Master) - Praktikum 1 & 2

Prof. Dr. rer. nat. Faßbender - FH Aachen

Author: Fabian Schöttler, 11/2021

https://bitbucket.org/studyrepos/ti2021-p1/

#### Usage
Use run.sh to compile and run on given program file.

-> $ ./run.sh [-b] [filename]

If no filename is given, it will run on res/example.txt .

Set -b to rebuild before run.

#### Dependencies
- JavaCC
- Java (>= 9)

#### License
The MIT License (MIT)

Copyright (c) 2021 Fabian Schoettler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
