/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/


PARSER_BEGIN(While2URM)

import java.io.*;
import java.util.*;

public class While2URM {
    static String name;
    static String headline;

    static int variable_index = 2;
    static HashMap<String, String> variables = new HashMap<>();

    static List<String> urm = new ArrayList<>();

    /** Main entry point. */
    public static void main(String... args) throws ParseException {
        if (args.length < 1) {
            System.out.println("Missing input filename");
            return;
        }

        try {
            While2URM parser = new While2URM(new FileInputStream(args[0]));
            System.out.println("Start parser...");
            parser.Program();
            String target = String.format("./target/%s.urm", name);
            System.out.println((char)27 + "[32m" + "SUCCESS:" + (char)27 + "[39m" + " Program valid -> " + target);

            FileWriter writer = new FileWriter(target);
            writer.write(String.format("%s%n", headline));

            writer.write("; -------------\n");
            writer.write("; Variable -> Register:\n");
            for (java.util.Map.Entry<String, String> e : variables.entrySet())
                writer.write(String.format("; %s -> %s%n", e.getKey(), e.getValue()));
            writer.write("; -------------\n");

            for (String s : urm)
                writer.write(String.format("%s%n", s));

            writer.close();

        } catch (FileNotFoundException e) {
            System.err.println((char)27 + "[31m" + "ERROR:" + (char)27 + "[39m" + " File not found");
            System.err.println(e.getMessage());
        } catch (ParseException | TokenMgrError e) {
            System.err.println(e);
            System.err.println((char)27 + "[31m" + "ERROR:" + (char)27 + "[39m" + " Not a valid WHILE0-Program");
        } catch (Exception e) {
            System.err.println((char)27 + "[31m" + "ERROR:" + (char)27 + "[39m" + " Something went wrong....");
            System.err.println(e.getMessage());
        }
    }
}
PARSER_END(While2URM)

SKIP :
{
      " "
    | "\t"
    | "\r"
    | "\n"
}

TOKEN:
{
      < INC : "+" >
    | < ONE : "1" >
    | < ZERO : "0" >
    | < SEMICOLON : ";" >
    | < COMMA : "," >
    | < EQUALS : "=" >
    | < LPAREN : "(" >
    | < RPAREN : ")" >
    | < UNEQUAL : "!=">
    | < IN : "in" >
    | < DO : "do" >
    | < OUT : "out" >
    | < END : "end" >
    | < VARS : "var" >
    | < WHILE : "while" >
    | < BEGIN : "begin" >
    | < IDENT : ["a"-"z","A"-"Z"] ( ["a"-"z","A"-"Z","0"-"9"] )* >
}

void Program() : {}
{
   Head()
   Variables()
   Sequence()
   <EOF>
}

void Head() : {
    Token program_name, var_out;
    List<String> var_in;
 }
{
   program_name=<IDENT> <LPAREN> <IN>
   var_in=VariableList() <SEMICOLON> <OUT>
   var_out=<IDENT> <RPAREN> <SEMICOLON>{
        name = program_name.image;
        headline = String.format("; %s (", program_name);
        headline += String.join(", ", var_in);
        headline += String.format(" -> %s)", MapOutput(var_out));
   }
}

List<String> VariableList() :{
    Token var;
    List<String> vars = new ArrayList<>();
}
{
    var=<IDENT>{
        vars.add(MapVariable(var));
    }
    (<COMMA> var=<IDENT> {
        vars.add(MapVariable(var));
    }
    )* {
        return vars;
    }
}

void Variables() : {
    List<String> vars;
}
{
    <VARS> <LPAREN> vars=VariableList() <RPAREN> <SEMICOLON> {
        for (String t : vars) {
            urm.add(String.format("%s = 0; init", t));
        }
    }
}

void Sequence() : {}
{
    (Assign() | Loop()) [ <SEMICOLON> Sequence() ]
}

void Assign() : {
  String lvar, rvar;
}
{
    lvar=Identifier() <EQUALS> (<ZERO> {
        urm.add(String.format("%s = 0;", lvar));
    }

    | (rvar=Identifier() <INC> <ONE> {
          if (!lvar.equals(rvar))
              mCopy(rvar, lvar);
          urm.add(String.format("%s++;", lvar));
        }
    ))
}

void Loop() : {
    String lvar, rvar, lcopy, rcopy;
    int jump_outer, jump_inner;
}
{
    <WHILE> lvar=Identifier() <UNEQUAL> rvar=Identifier() <DO> <BEGIN> {
        int line_loop_start = urm.size();
        lcopy = GetUtilRegister();
        rcopy = GetUtilRegister();

        mCopy(lvar, lcopy);
        mCopy(rvar, rcopy);

        urm.add(String.format("if %s == 0 goto %d; inner loop", lcopy, (line_loop_start + 25)));
        urm.add(String.format("goto %d;", (line_loop_start + 27)));
        urm.add(String.format("if %s != 0 goto %d; alpha", rcopy, (line_loop_start + 31)));
        // goto end , replaced later at line_loop_start + 26 in urm-list
        urm.add("goto end");
        urm.add(String.format("if %s == 0 goto %d; alpha", rcopy, (line_loop_start + 31)));
        urm.add(String.format("%s--;", lcopy));
        urm.add(String.format("%s--;", rcopy));
        urm.add(String.format("goto %d; next inner iteration", (line_loop_start + 23)));

    }
    Sequence()
    <END> {
        urm.add(String.format("goto %d; next outer iteration #END-WHILE", (line_loop_start + 1)));
        int line_end = urm.size();
        urm.add(line_loop_start + 25, String.format("goto %d; condition check -> end", (line_end + 1)));
        urm.remove(line_loop_start + 26);

        FreeUtilRegister();
        FreeUtilRegister();
    }
}

String Identifier() : {
    Token var;
    String reg;
}
{
    var=<IDENT> {
        reg = variables.get(var.image);
        if(reg == null)
            throw new ParseException(String.format(
                            "Unknown identifier \"%s\" at line %d, column %d.",
                            var.image, var.beginLine, var.beginColumn));
        return reg;
    }
}

JAVACODE
/**
* Copy lvar to rvar, keeping lvar
*/
void mCopy(String lvar, String rvar) {
    int line = urm.size();
    String temp = GetUtilRegister();
    urm.add(String.format("%s = 0; init temp register #START-COPY", temp));
    urm.add(String.format("%s = 0; init target", rvar));
    urm.add(String.format("if %s == 0 goto %d; decrement src to get value", lvar, (line + 7)));
    urm.add(String.format("%s--;", lvar));
    urm.add(String.format("%s++;", temp));
    urm.add(String.format("goto %d; next decrement iteration", (line + 3)));
    urm.add(String.format("if %s == 0 goto %d; increment until src and dst on value", temp, (line + 12)));
    urm.add(String.format("%s--;", temp));
    urm.add(String.format("%s++;", lvar));
    urm.add(String.format("%s++;", rvar));
    urm.add(String.format("goto %d; next increment iteration #END-COPY", (line + 7)));
    FreeUtilRegister();
}

JAVACODE
/**
* Register identifier as variable to urm-register
*/
String MapOutput(Token var) {
    return MapRegister(var, 1);
 }

JAVACODE
String MapVariable(Token var){
    return MapRegister(var, variable_index++);
}

JAVACODE
String MapRegister(Token var, int register) {
    if (variables.containsKey(var.image)){
        throw new ParseException(String.format(
                "Repeated identifier declaration \"%s\" at line %d, column %d.",
                var.image, var.beginLine, var.beginColumn));
    }
    String reg = String.format("R%d", register);
    variables.put(var.image, reg);
    return reg;
}

JAVACODE
String GetUtilRegister() {
    return String.format("R%d", variable_index++);
}

JAVACODE
void FreeUtilRegister() {
    if (variable_index > variables.size() + 1)
        --variable_index;
}
